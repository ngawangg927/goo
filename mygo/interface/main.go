package main

import "fmt"

type englishBot struct {}
type dzongkhaBot struct {}
type bot interface {
	greet() string
}

func main () {
	eb := englishBot{}       //Create two instences
	db := dzongkhaBot{}

	printGreeting(eb)
	printGreeting(db)
}

func (englishBot) greet () string {
	return "Hello"    //Custom logic
}

func (dzongkhaBot) greet () string {
	return "སྐུ་གཟུགས་བཟང་པོ་ལགས།།"    //Custom logic
}

// //You can ignore receiver variable if it is not used in the function
// func printGreeting (eb englishBot) {
// 	fmt.Println(eb.greet())
// }

// func printGreeting (db dzongkhaBot) {
// 	fmt.Println(db.greet())
// }

func printGreeting(b bot) {
	fmt.Println(b.greet())
}