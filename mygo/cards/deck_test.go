package main

import (
	"os"
	"testing"
)

// func TestNewDeck(t *testing.T) {
// 	d := newDeck()
// 	if len(d) != 16 {
// 		t.Errorf("expected deck length of 20 but got %v",len(d))
// 	}
// 	if d[0] != "Ace of spades " {
// 		t.Errorf("expected Ace of spades but found %v",d[0])
// 	}
// 	if d[len(d)-1] != "Four of Club" {
// 		t.Errorf("expected Four of Club but found %v",d[len(d)-1])
// 	}
// }

// %v general value store
// %d type integer
// %s string type
func TestNewDeck(t *testing.T){
	de := newDeck()
	if len(de) != 24 {
		t.Errorf("Expected deck to be 24, but got %v ",len(de))
	}

	if de[0] != "ace of club" {
		t.Errorf("The first card is not ace of club, it is %v",de[0])
	}
	if de[len(de)-1] != "five of hearts" {
		t.Errorf("The last card is not five of hearts, it is %v",de[len(de)-1])
	}
}
func TestSaveToFileAndNewDeckFromFile(t *testing.T){
	de :=newDeck()
	os.Remove("_decktesting")
	de.saveTofile("_decktesting")

	deck :=newDeckFromFile("_decktesting")
	if len(deck) != 24 {
		t.Errorf("The length of the deck is not 24, it is %v",len(deck))
	}
	os.Remove("_decktesting")
}