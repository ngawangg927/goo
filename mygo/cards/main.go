package main

////** export the value to the main
// func main() {//block of code which performs a specific task
// 	var card string ="ACE of spades" //explicit (Type is declared )
// 	// card := "Ace of spades" //implecit (auto determine the type )
// 	card ="five f Diamond"//assign value to the existing variable
// 	fmt.Println(card)
// }
func main (){
	cards := newDeck()
	cards.shuffle()
	cards.print()


	// card := newCard()
	// card := []string{"Ace of Diamond","five of spadesd"}
	// fmt.Println(card)
	// func make ([]type,length,capacity)
// cards := []string{"Ace of Diamond ",newCard()}//the []string is =deck now 
// cards := deck{" Ace of Diamond ",newCard()}
// cards = append(cards, "six of spades")
// cards.print()

// cards := newDeck()
// hand,remainingcards := deal(cards,5)
// hand.print()
// remainingcards.print()

// cards := newDeck()
// fmt.Println(cards.toString())

// cards := newDeck()
// cards.saveTofile("my_card")

// cards := newDeckFromFile("my_cards")
// cards.print()

// cards := newDeck()
// cards.print()
// fmt.Println(cards)

// for i,cards := range cards{
// 	fmt.Println(i,cards)
// }

//just to check the length 
// fmt.Println(len(newDeck()))

}
// Create a new function (newCard) that returns a value for a card variable rather than
// assigning it directly in the main.
//return_type, it is mandatory to have a return statement in
//the function
// func newCard()string{
// 	return "Ace of Spades "
// }

