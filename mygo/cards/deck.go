package main

import (
	"fmt"
	"math/rand"
	"os"
	"strings"
)

type deck []string 

//receiver function 
func (d deck )print(){
	for i,card :=range d {
		fmt.Println(i,card)
	}
}
func newDeck() deck{
	cards :=deck{} // empty deck 
	cardsSuits := []string{"spade","heart","Diamond","clubs"}
	cardsValues := []string{"Ace","two","three","four"}
	//to avoid index printing
	//for index,name := range name{}
	for _,suit := range cardsSuits{
		for _,value := range cardsValues{
			cards =append(cards,value +" of "+ suit)
		}
	}
	return cards
}

//slice[startIndex : endIndex]
//func function_name(Parameter-list) (Return_type) {
// function body.....}

func deal(d deck ,handsize int )(deck,deck){
	return d [:handsize],d[handsize:]
}

func(d deck)toString()string{
	return strings.Join([]string(d),",")
}
func (d deck)saveTofile(filename string)error{
	return os.WriteFile(filename,[]byte(d.toString()),0666)
}
func newDeckFromFile (fileName string)deck{
	byteSlice, err:= os.ReadFile(fileName)
	if err != nil{
		fmt.Println("error",err)
		os.Exit(1)
	}
	str := strings.Split(string(byteSlice),",")
	return deck(str)
}

func (d deck) shuffle()deck{
	for i := range d {
		newIndex := rand.Intn(len(d)-1)

		d[i], d[newIndex] = d[newIndex], d[i]
	}
	return d
}