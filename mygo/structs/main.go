package main

import "fmt"

type person struct {
	firstName string
	LastName  string
	address  contactInfo
}
type contactInfo struct{
	email string 
	mobile int 
}
func main(){
	
    var x int = 42
    fmt.Printf("Address of x: %p", &x)
	// formatting for printing the memory address 



    // x := 42

    // // Decimal format
    // fmt.Printf("Decimal: %d\n", x)

    // // Binary format
    // fmt.Printf("Binary: %b\n", x)

    // // Octal format
    // fmt.Printf("Octal: %o\n", x)

    // // Hexadecimal format
    // fmt.Printf("Hexadecimal: %x\n", x)

    // // Scientific notation format
    // y := 3.14159265359
    // fmt.Printf("Scientific Notation: %e\n", y)

	// x := 42

    // // Minimum width of 5, zero-padded
    // fmt.Printf("Padded: %05d\n", x)

    // // Precision of 2 for floating point
    // y := 3.14159265359
    // fmt.Printf("Rounded: %.2f\n", y)



	// //perspn := person ("N","G")
	// person1 := person{firstName:"Ngawang",LastName:"Gyelthen"}
	// fmt.Println(person1)
	
	// //ver variable type 
	// var NG person 
	// NG.firstName="kezang"
	// NG.LastName="samba "
	// fmt.Println(NG)
	// fmt.Printf("%+v",NG)

    NG := person {
		firstName:"thering",
		LastName:"choden",
		address: contactInfo{
			email:"ngawang927@gmail.com",
			mobile: 17495130,
		},
	}
	// NGPointer := &NG //get the memory address 
	// NG.print()
	// NGPointer.updateName("Kezang")
	// NG.print()
	// // NG.updateName("Kencho")
	NG.updateName("sujal")
	NG.print()

	mySlice := []string{"hello","how","are","you"}
	updateSlice(mySlice)
	fmt.Println(mySlice)
}

func updateSlice(s []string){
	s[0]="hi"
}
func (p person) print(){ 
	fmt.Printf("%+v",p)
}

func (pPointer *person) updateName(newfirstName string){ //get the value stored in thus memory address 
	(pPointer).firstName=newfirstName
}








// func(P person) updateName(newfirstName string){
// 	p.firstName = newfirstName
// }
	
	// struct pointer helps u to update the original value stored in a variable 
