package main

import (
	"fmt"
	"net/http"
	"os"
)

func main() {
	resp,err := http.Get("http://google.com")
	if err != nil {
		//error heading
		fmt.Println("Error:",err)
		os.Exit(1)
	}
	// no error 
	// fmt.Println(resp)

	bs := make([]byte,99999)
   	resp.Body.Read(bs)
	fmt.Println(string(bs))
}
// package main

// import (
// 	"fmt"
// 	"net/http"
// 	"os"
// 	"io"
// )

// func main() {
// 	resp,err := http.Get("http://google.com")
// 	if err != nil {
// 		//error heading
// 		fmt.Println("Error:",err)
// 		os.Exit(1)
// 	}
// 	defer resp.Body.Close()

// 	// using io.Copy to write response body to os.Stdout
// 	io.Copy(os.Stdout, resp.Body)
// }
