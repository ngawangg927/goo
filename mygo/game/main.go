package main

import (
	"fmt"
)

func main() {
    // Initialize the game board
    board := [3][3]string{
        {"-", "-", "-"},
        {"-", "-", "-"},
        {"-", "-", "-"},
    }

    // Print the initial game board
    printBoard(board)

    // Keep track of whose turn it is
    var player string = "X"

    // Start the game loop
    for {
        // Ask the player to make a move
        fmt.Printf("Player %s, make your move (row column): ", player)
        var row, col int
        fmt.Scanln(&row, &col)

        // Make sure the move is valid
        if row < 0 || row > 2 || col < 0 || col > 2 {
            fmt.Println("Invalid move. Please try again.")
            continue
        } else if board[row][col] != "-" {
            fmt.Println("That spot is already taken. Please try again.")
            continue
        }

        // Update the game board with the player's move
        board[row][col] = player

        // Print the updated game board
        printBoard(board)

        // Check if the game is over
        if checkWin(board, player) {
            fmt.Printf("Player %s wins!\n", player)
            break
        } else if checkTie(board) {
            fmt.Println("It's a tie!")
            break
        }

        // Switch to the other player's turn
        if player == "X" {
            player = "O"
        } else {
            player = "X"
        }
    }
}

func printBoard(board [3][3]string) {
    fmt.Println("   0 1 2")
    for i := 0; i < 3; i++ {
        fmt.Printf("%d ", i)
        for j := 0; j < 3; j++ {
            fmt.Printf("%s ", board[i][j])
        }
        fmt.Println()
    }
}

func checkWin(board [3][3]string, player string) bool {
    // Check rows
    for i := 0; i < 3; i++ {
        if board[i][0] == player && board[i][1] == player && board[i][2] == player {
            return true
        }
    }

    // Check columns
    for j := 0; j < 3; j++ {
        if board[0][j] == player && board[1][j] == player && board[2][j] == player {
            return true
        }
    }

    // Check diagonals
    if board[0][0] == player && board[1][1] == player && board[2][2] == player {
        return true
    }
    if board[0][2] == player && board[1][1] == player && board[2][0] == player {
        return true
    }

    return false
}

	func checkTie(board [3][3]string) bool {
	    for i := 0; i < 3; i++ {
	        for j := 0; j < 3; j++ {
	            if board[i][j] == "-" {
	                return false
	            }
	        }
	    }
	    return true
	}
